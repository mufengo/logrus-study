package logs

import "io"
// 存初始化方法而不直接存Writer的好处
// 只需要初始化一次
var writerAdapter = make(map[string]InitLogWriterFunc,0)

type InitLogWriterFunc func() LogWriter

type LogWriter interface {
	Flush()
	io.Writer
}

func RegisterInitWriterFunc(adapterName string,writerFunc InitLogWriterFunc)  {
	writerAdapter[adapterName] = writerFunc
}
