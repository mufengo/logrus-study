package logs

import "os"

// 控制台输出Writer

type stdWriter struct {
	*os.File
}

func (s *stdWriter) Flush()  {
	_ = s.Sync()
}

func newStdWriter() LogWriter {
	return &stdWriter{
		os.Stdout,
	}
}

func init() {
	RegisterInitWriterFunc("std",newStdWriter)
}