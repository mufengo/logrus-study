package logs

import (
	"github.com/sirupsen/logrus"
)

type Log struct {
	*logrus.Entry
	LogWriter
}

// 日志配置
type LogConf struct {
	AdapterName string // 日志输出类型
	Level logrus.Level // 日志输出等级
}

func (log *Log) Flush() {
	log.LogWriter.Flush()
}

type myHook struct {}

func (hook *myHook) Levels() []logrus.Level {
	return logrus.AllLevels
}

func (hook *myHook) Fire(entry *logrus.Entry) error {
	entry.Data["author"] = "hqf" // 添加信息
	return nil
}

func InitLog(conf *LogConf) (log *Log){
	if conf.AdapterName == "" {
		conf.AdapterName = "std" // 默认标准控制台输出
	}
	writerInitFunc,ok := writerAdapter[conf.AdapterName]
	if ok {
		log = &Log{
			logrus.NewEntry(logrus.New()),
			writerInitFunc(),
		}
		log.Logger.SetFormatter(&logrus.JSONFormatter{})
		log.Logger.SetReportCaller(true) // 打印行号
		log.Logger.SetOutput(log.LogWriter)
		if conf.Level != 0 {
			log.Logger.SetLevel(conf.Level)
		}
		// Hook 钩子函数
		log.Logger.AddHook(&myHook{})
	}
	return
}