package logs

import (
	"os"
)

// 文件输出Writer

const LOG_PATH = "runtime/logs/logrus.log"

type fileWriter struct {
	*os.File
}

func (f *fileWriter) Flush()  {
	_ = f.Sync()
}

func newFileWriter() LogWriter {
	file,err := os.OpenFile(LOG_PATH,os.O_CREATE|os.O_APPEND|os.O_WRONLY,0666)
	if err != nil {
		file = os.Stderr
	}
	return &fileWriter{
		file,
	}
}

func init() {
	RegisterInitWriterFunc("file",newFileWriter)
}