package logs

import (
	rotatelogs "github.com/lestrrat-go/file-rotatelogs"
	"time"
)

// 文件拆分输出Writer

type fileRotateWriter struct {
	*rotatelogs.RotateLogs
}

func (frw *fileRotateWriter) Flush()  {
	_ = frw.Close()
}

func newFileRotateWriter() LogWriter {
	path := LOG_PATH
	logf, err := rotatelogs.New(
		path + "-%Y%m%d%H%M",
		// rotatelogs.WithLinkName(path),
		rotatelogs.WithMaxAge(24 * time.Hour), // 最大保留时间
		rotatelogs.WithRotationTime(time.Hour), // 分隔时间
	)
	if err != nil {
		return newStdWriter()
	}
	return &fileRotateWriter{
		logf,
	}
}

func init() {
	RegisterInitWriterFunc("fileRotate",newFileRotateWriter)
}