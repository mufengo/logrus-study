package logs

import (
	"fmt"
)

// 自定义输出Writer

type cusWriter struct {
}

func (c *cusWriter) Flush()  {
}

// 日志输出目标完全取决于Write
func (c *cusWriter) Write(p []byte) (n int, err error){
	return fmt.Println("【",string(p),"】")
}

func newCusWriter() LogWriter {
	return &cusWriter{}
}

func init() {
	RegisterInitWriterFunc("cus",newCusWriter)
}