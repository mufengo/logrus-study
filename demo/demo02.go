package main

import (
	"github.com/sirupsen/logrus"
	"os"
)

var log = logrus.NewEntry(logrus.New())

func init() {
	log.Logger.SetOutput(os.Stdout)
	log.Logger.SetFormatter(&logrus.JSONFormatter{})
	log.Logger.SetLevel(logrus.InfoLevel)
	log.Logger.SetReportCaller(true) // 打印行号
}

func main() {
	log.Info("Hello World")
}