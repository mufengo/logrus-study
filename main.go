package main

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"logrus-study/logs"
	"net/http"
	"time"
)

// 测试标准控制台输出Writer
func testStdWriter() {
	// 初始化日志
	logConf := logs.LogConf{
		AdapterName: "std",
		Level: logrus.InfoLevel,
	}
	log := logs.InitLog(&logConf)
	log.Logger.Info("Hello World")
}

// 测试文件输出Writer
func testFileWriter() {
	// 初始化日志
	logConf := logs.LogConf{
		AdapterName: "file",
		Level: logrus.InfoLevel,
	}
	log := logs.InitLog(&logConf)
	log.Logger.Info("Hello World")
}

// 测试文件拆分输出Writer
func testFileRotateWriter() {
	// 初始化日志
	logConf := logs.LogConf{
		AdapterName: "fileRotate",
		Level: logrus.InfoLevel,
	}
	log := logs.InitLog(&logConf)
	log.Logger.Info("Hello World")
}

// 测试自定义输出Writer
func testCusWriter() {
	// 初始化日志
	logConf := logs.LogConf{
		AdapterName: "cus",
		Level: logrus.InfoLevel,
	}
	log := logs.InitLog(&logConf)
	log.Logger.Info("Hello World")
}

var Log *logs.Log

// 测试用logrus替换gin框架日志打印
func testReplaceGinLogger(){
	logConf := logs.LogConf{
		AdapterName: "file",
		Level: logrus.InfoLevel,
	}
	Log = logs.InitLog(&logConf)
	//r := gin.Default()
	r := gin.New()
	r.Use(myLogger) // 使用自定义日志中间件
	r.Use(gin.Recovery())

	r.GET("/ping", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{
			"message": "pong",
		})
	})
	_ = r.Run() // listen and serve on 0.0.0.0:8080 (for windows "localhost:8080")
}

func myLogger(c *gin.Context) {
	// Start timer
	start := time.Now()
	path := c.Request.URL.Path
	raw := c.Request.URL.RawQuery
	if raw != "" {
		path = path + "?" + raw
	}
	// Process request
	c.Next()
	latency := time.Since(start).Milliseconds()
	clientIp := c.ClientIP()
	method := c.Request.Method
	statusCode := c.Writer.Status()
	bodySize := c.Writer.Size()
	Log.WithFields(logrus.Fields{
		"path": 		path,
		"client_ip":	clientIp,
		"method": 		method,
		"status_code": 	statusCode,
		"size": 		bodySize,
		"latency": 		latency,
	}).Info()
}

func main() {
	//testStdWriter()
	//testFileWriter()
	//testCusWriter()

	//testFileRotateWriter()
	testReplaceGinLogger()
}
